<?php
/**
 * @file
 * patched.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function patched_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function patched_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function patched_node_info() {
  $items = array(
    'patch' => array(
      'name' => t('Patch'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
