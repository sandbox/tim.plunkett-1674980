<?php
/**
 * @file
 * patched.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function patched_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'patches';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Patches';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Patches';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_d_o_project' => 'field_d_o_project',
    'field_version' => 'field_d_o_project',
    'field_d_o_nid' => 'field_d_o_nid',
    'title' => 'title',
    'field_status' => 'field_status',
    'field_priority' => 'field_priority',
    'field_category' => 'field_category',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'field_d_o_project' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => ' ',
      'empty_column' => 0,
    ),
    'field_version' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_d_o_nid' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_status' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_priority' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_category' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_patch_issue_target_id']['id'] = 'field_patch_issue_target_id';
  $handler->display->display_options['relationships']['field_patch_issue_target_id']['table'] = 'field_data_field_patch_issue';
  $handler->display->display_options['relationships']['field_patch_issue_target_id']['field'] = 'field_patch_issue_target_id';
  $handler->display->display_options['relationships']['field_patch_issue_target_id']['label'] = 'Issue';
  $handler->display->display_options['relationships']['field_patch_issue_target_id']['required'] = TRUE;
  /* Field: Content: d.o project */
  $handler->display->display_options['fields']['field_d_o_project']['id'] = 'field_d_o_project';
  $handler->display->display_options['fields']['field_d_o_project']['table'] = 'field_data_field_d_o_project';
  $handler->display->display_options['fields']['field_d_o_project']['field'] = 'field_d_o_project';
  $handler->display->display_options['fields']['field_d_o_project']['relationship'] = 'field_patch_issue_target_id';
  $handler->display->display_options['fields']['field_d_o_project']['label'] = 'Project';
  $handler->display->display_options['fields']['field_d_o_project']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_d_o_project']['alter']['path'] = 'http://drupal.org/project/[field_d_o_project]';
  $handler->display->display_options['fields']['field_d_o_project']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['field_d_o_project']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['field_d_o_project']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Version */
  $handler->display->display_options['fields']['field_version']['id'] = 'field_version';
  $handler->display->display_options['fields']['field_version']['table'] = 'field_data_field_version';
  $handler->display->display_options['fields']['field_version']['field'] = 'field_version';
  $handler->display->display_options['fields']['field_version']['relationship'] = 'field_patch_issue_target_id';
  $handler->display->display_options['fields']['field_version']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_version']['alter']['path'] = 'http://drupal.org/project/[field_d_o_project]';
  $handler->display->display_options['fields']['field_version']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['field_version']['alter']['external'] = TRUE;
  $handler->display->display_options['fields']['field_version']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: d.o nid */
  $handler->display->display_options['fields']['field_d_o_nid']['id'] = 'field_d_o_nid';
  $handler->display->display_options['fields']['field_d_o_nid']['table'] = 'field_data_field_d_o_nid';
  $handler->display->display_options['fields']['field_d_o_nid']['field'] = 'field_d_o_nid';
  $handler->display->display_options['fields']['field_d_o_nid']['relationship'] = 'field_patch_issue_target_id';
  $handler->display->display_options['fields']['field_d_o_nid']['label'] = 'Issue';
  $handler->display->display_options['fields']['field_d_o_nid']['type'] = 'd_o_issue_importer_link';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'field_patch_issue_target_id';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Status */
  $handler->display->display_options['fields']['field_status']['id'] = 'field_status';
  $handler->display->display_options['fields']['field_status']['table'] = 'field_data_field_status';
  $handler->display->display_options['fields']['field_status']['field'] = 'field_status';
  $handler->display->display_options['fields']['field_status']['relationship'] = 'field_patch_issue_target_id';
  /* Field: Content: Priority */
  $handler->display->display_options['fields']['field_priority']['id'] = 'field_priority';
  $handler->display->display_options['fields']['field_priority']['table'] = 'field_data_field_priority';
  $handler->display->display_options['fields']['field_priority']['field'] = 'field_priority';
  $handler->display->display_options['fields']['field_priority']['relationship'] = 'field_patch_issue_target_id';
  /* Field: Content: Category */
  $handler->display->display_options['fields']['field_category']['id'] = 'field_category';
  $handler->display->display_options['fields']['field_category']['table'] = 'field_data_field_category';
  $handler->display->display_options['fields']['field_category']['field'] = 'field_category';
  $handler->display->display_options['fields']['field_category']['relationship'] = 'field_patch_issue_target_id';
  /* Field: Content: Site */
  $handler->display->display_options['fields']['field_patch_site']['id'] = 'field_patch_site';
  $handler->display->display_options['fields']['field_patch_site']['table'] = 'field_data_field_patch_site';
  $handler->display->display_options['fields']['field_patch_site']['field'] = 'field_patch_site';
  $handler->display->display_options['fields']['field_patch_site']['delta_offset'] = '0';
  /* Field: Node: Closed/Fixed */
  $handler->display->display_options['fields']['patched_closed']['id'] = 'patched_closed';
  $handler->display->display_options['fields']['patched_closed']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['patched_closed']['field'] = 'patched_closed';
  $handler->display->display_options['fields']['patched_closed']['relationship'] = 'field_patch_issue_target_id';
  $handler->display->display_options['fields']['patched_closed']['not'] = 0;
  $handler->display->display_options['fields']['patched_closed']['link_to_entity'] = 0;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'patch' => 'patch',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'patches';
  $export['patches'] = $view;

  return $export;
}
